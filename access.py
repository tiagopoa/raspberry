
from flask import Flask
from flask_restful import Resource, Api
from flask_restful import reqparse
from flaskext.mysql import MySQL


mysql = MySQL()
app = Flask(__name__)


# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'pi'
app.config['MYSQL_DATABASE_PASSWORD'] = 'senac123'
app.config['MYSQL_DATABASE_DB'] = 'gunshotdb'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'


mysql.init_app(app)

api = Api(app)



class GetAllItems(Resource):
    def post(self):
        try: 
            # Parse the arguments
            parser = reqparse.RequestParser()
            parser.add_argument('id', type=str)
            args = parser.parse_args()

            _userId = args['id']

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_GetAllItems',(_userId,))
            data = cursor.fetchall()

            items_list=[];
            for item in data:
                i = {
                    'Id':item[0],
                    'Item':item[1]
                }
                items_list.append(i)

            return {'StatusCode':'200','Items':items_list}

        except Exception as e:
            return {'error': str(e)}

class CreateAccess(Resource):
    def post(self):
        try:
            # Parse the arguments
            parser = reqparse.RequestParser()
            parser.add_argument('person', type=str, help='User name')
            parser.add_argument('status', type=bool, help='Door status')
            parser.add_argument('date', type=str, help='Date')
            parser.add_argument('pic', type=str, help='Image')
            parser.add_argument('pic64', type=str, help='Image base 64')
            
            args = parser.parse_args()

            _person = args['person']
            _status = args['status']
            _date = args['date']
            _pic = args['pic']
            _pic64 = args['pic64']
                        
            conn = mysql.connect()            
            cursor = conn.cursor()            
            cursor.callproc('addAccess',(_person,_status,_date,_pic,_pic64))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return {'StatusCode':'200','Message': 'Access creation success'}
            else:
                return {'StatusCode':'1000','Message': str(data[0])}

        except Exception as e:
            return {'error': str(e)}

api.add_resource(CreateAccess, '/CreateAccess')
#api.add_resource(AuthenticateUser, '/AuthenticateUser')
#api.add_resource(AddItem, '/AddItem')
api.add_resource(GetAllItems, '/GetAllItems')        


if __name__ == '__main__':
    app.run(debug=True)

